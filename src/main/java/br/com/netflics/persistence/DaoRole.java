package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.netflics.core.persistence.AccessibleHibernateDao;
import br.com.netflics.core.persistence.pagination.Pagination;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.persistence.pagination.Paginator;
import br.com.netflics.model.Role;
import br.com.netflics.model.filter.FilterRole;

/* generated by JSetup v0.94 :  at 23/06/2020 21:13:10 */
@Named
@SuppressWarnings("rawtypes")
public class DaoRole extends AccessibleHibernateDao<Role> {
	private static final Logger LOGGER = Logger.getLogger(DaoRole.class);

	public DaoRole() {
		super(Role.class);
	}
	public Role findByAuthority(String authority) {
		Role role = null;
		try {
			role = (Role) criteria().add(Restrictions.eq("authority", authority)).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter Papel pelo authority," + authority, e);
		}
		return role;
	}
	public Role findByDescription(String description) {
		Role role = null;
		try {
			role = (Role) criteria().add(Restrictions.eq("description", description)).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter Papel pelo description," + description, e);
		}
		return role;
	}

	@Override
	public Pagination<Role> getAll(PaginationParams paginationParams) {
		FilterRole filterRole = (FilterRole) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterRole.getAuthority() != null) {
			searchCriteria.add(Restrictions.ilike("authority", filterRole.getAuthority(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("authority", filterRole.getAuthority(), MatchMode.ANYWHERE));
		}
		if (filterRole.getDescription() != null) {
			searchCriteria.add(Restrictions.ilike("description", filterRole.getDescription(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("description", filterRole.getDescription(), MatchMode.ANYWHERE));
		}

		return new Paginator<Role>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Role> filter(PaginationParams paginationParams, Boolean equals) {
		List<Role> list = new ArrayList<Role>();
		FilterRole filterRole = (FilterRole) paginationParams.getFilter();
		
		return filter(filterRole, equals);
	}
	
	
	public List<Role> filter(FilterRole filterRole,  Boolean equals) {
		if (equals) {
			return filterEqual(filterRole);
		} else {
			return filterAlike(filterRole);
		}
	}
	
	
	public List<Role> filterEqual(FilterRole filterRole) {
		List<Role> list = new ArrayList<Role>();
		Criteria searchCriteria = criteria();
		if (filterRole.getAuthority() != null) {
			searchCriteria.add(Restrictions.eq("authority", filterRole.getAuthority()));
		}				
		if (filterRole.getDescription() != null) {
			searchCriteria.add(Restrictions.eq("description", filterRole.getDescription()));
		}				
		// max 100 rows
		list.addAll(searchCriteria.setMaxResults(100).list());
		return list;
	}
	
	public List<Role> filterAlike(FilterRole filterRole) {
		List<Role> list = new ArrayList<Role>();
		Criteria searchCriteria = criteria();
		if (filterRole.getAuthority() != null) {
			searchCriteria.add(Restrictions.ilike("authority", filterRole.getAuthority(), MatchMode.ANYWHERE));
		}
		if (filterRole.getDescription() != null) {
			searchCriteria.add(Restrictions.ilike("description", filterRole.getDescription(), MatchMode.ANYWHERE));
		}
		// max 100 rows
		list.addAll(searchCriteria.setMaxResults(100).list());
		return list;
	}	
	
}

//generated by JSetup v0.94 :  at 23/06/2020 21:13:10