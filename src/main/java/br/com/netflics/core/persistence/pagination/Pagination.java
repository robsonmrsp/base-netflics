package br.com.netflics.core.persistence.pagination;

import java.util.List;
/* generated by JSetup v0.94 :  at 23/06/2020 21:13:09 */
public class Pagination<T> {

	private final Long totalRecords;
	private final List<T> results;

	public Pagination(Long totalRecords, List<T> results) {
		this.totalRecords = totalRecords;
		this.results = results;
	}

	public List<T> getResults() {
		return results;
	}

	public Long getTotalRecords() {
		return totalRecords;
	}
}