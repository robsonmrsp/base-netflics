package br.com.netflics.core.reports;

import java.util.LinkedList;

/* generated by JSetup v0.94 :  at 23/06/2020 21:13:09 */
public abstract class CSVLayout<T> {

	private LinkedList<CSVColumn<T>> columns = new LinkedList<CSVColumn<T>>();

	public void addColumn(CSVColumn<T> csvColumn) {
		getColumns().add(csvColumn);
	}

	public LinkedList<CSVColumn<T>> getColumns() {
		return columns;
	}

	public void setColumns(LinkedList<CSVColumn<T>> columns) {
		this.columns = columns;
	}
	
	public abstract void adicionarColunas();
}
