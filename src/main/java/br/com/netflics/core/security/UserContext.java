package br.com.netflics.core.security;

import br.com.netflics.model.User;
/**
*  generated by JSetup v0.94 :  at 23/06/2020 21:13:09
**/

public interface UserContext {

	User getCurrentUser();

	String getCurrentUserName();
	
	void setCurrentUser(User user);
}
