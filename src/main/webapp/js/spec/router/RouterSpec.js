define(function(require) {
	var Router = require('Router');
	describe("Rotas", function() {

		beforeEach(function() {
			try {
				Backbone.history.stop();
			} catch (e) {
				console.error(e);
			}
		});
		
		afterEach(function() {
			// Reset URL
			var router = new Router();
			router.navigate("");
		});
				it("Rota de \"Filmes\"", function() {
			spyOn(Router.prototype, "Filmes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Filmes', true);
			expect(Router.prototype.Filmes).toHaveBeenCalled();
		});

		it("Rota de \"newfilme\"", function() {
			spyOn(Router.prototype, "newfilme")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newfilme', true);
			expect(Router.prototype.newfilme).toHaveBeenCalled();
		});
		
		it("Rota de \"editfilme\"", function() {
			spyOn(Router.prototype, "editfilme")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editfilme/1', true);
			expect(Router.prototype.editfilme).toHaveBeenCalled();
		});
		it("Rota de \"Generos\"", function() {
			spyOn(Router.prototype, "Generos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Generos', true);
			expect(Router.prototype.Generos).toHaveBeenCalled();
		});

		it("Rota de \"newgenero\"", function() {
			spyOn(Router.prototype, "newgenero")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newgenero', true);
			expect(Router.prototype.newgenero).toHaveBeenCalled();
		});
		
		it("Rota de \"editgenero\"", function() {
			spyOn(Router.prototype, "editgenero")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editgenero/1', true);
			expect(Router.prototype.editgenero).toHaveBeenCalled();
		});
		it("Rota de \"Users\"", function() {
			spyOn(Router.prototype, "Users")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Users', true);
			expect(Router.prototype.Users).toHaveBeenCalled();
		});

		it("Rota de \"newuser\"", function() {
			spyOn(Router.prototype, "newuser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newuser', true);
			expect(Router.prototype.newuser).toHaveBeenCalled();
		});
		
		it("Rota de \"edituser\"", function() {
			spyOn(Router.prototype, "edituser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edituser/1', true);
			expect(Router.prototype.edituser).toHaveBeenCalled();
		});
		it("Rota de \"Roles\"", function() {
			spyOn(Router.prototype, "Roles")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Roles', true);
			expect(Router.prototype.Roles).toHaveBeenCalled();
		});

		it("Rota de \"newrole\"", function() {
			spyOn(Router.prototype, "newrole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newrole', true);
			expect(Router.prototype.newrole).toHaveBeenCalled();
		});
		
		it("Rota de \"editrole\"", function() {
			spyOn(Router.prototype, "editrole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editrole/1', true);
			expect(Router.prototype.editrole).toHaveBeenCalled();
		});
		it("Rota de \"Permissions\"", function() {
			spyOn(Router.prototype, "Permissions")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Permissions', true);
			expect(Router.prototype.Permissions).toHaveBeenCalled();
		});

		it("Rota de \"newpermission\"", function() {
			spyOn(Router.prototype, "newpermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newpermission', true);
			expect(Router.prototype.newpermission).toHaveBeenCalled();
		});
		
		it("Rota de \"editpermission\"", function() {
			spyOn(Router.prototype, "editpermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editpermission/1', true);
			expect(Router.prototype.editpermission).toHaveBeenCalled();
		});
		it("Rota de \"Groups\"", function() {
			spyOn(Router.prototype, "Groups")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Groups', true);
			expect(Router.prototype.Groups).toHaveBeenCalled();
		});

		it("Rota de \"newgroup\"", function() {
			spyOn(Router.prototype, "newgroup")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newgroup', true);
			expect(Router.prototype.newgroup).toHaveBeenCalled();
		});
		
		it("Rota de \"editgroup\"", function() {
			spyOn(Router.prototype, "editgroup")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editgroup/1', true);
			expect(Router.prototype.editgroup).toHaveBeenCalled();
		});
		it("Rota de \"Items\"", function() {
			spyOn(Router.prototype, "Items")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Items', true);
			expect(Router.prototype.Items).toHaveBeenCalled();
		});

		it("Rota de \"newitem\"", function() {
			spyOn(Router.prototype, "newitem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newitem', true);
			expect(Router.prototype.newitem).toHaveBeenCalled();
		});
		
		it("Rota de \"edititem\"", function() {
			spyOn(Router.prototype, "edititem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edititem/1', true);
			expect(Router.prototype.edititem).toHaveBeenCalled();
		});
	});
})
